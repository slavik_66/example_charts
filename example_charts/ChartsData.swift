//
//  ChartsData.swift
//  example_charts
//
//  Created by slavik_66 on 12/03/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import Foundation
import UIKit

class ChartsData {
    
    typealias ColorParam = [String:UIColor]
    typealias StringDictionary = [String:String]
    
    var columnsId: [String] = []
    var columns: [LineEntry] = []
    var xAxisValue: [Date] = []
    var types: StringDictionary = [:]
    var names: StringDictionary = [:]
    var colors: ColorParam = [:]
    var y_scaled = false
    var stacked = false
    var percentage = false
//    var lengh: Double = 0
    
    private lazy var stepX = { (lengh: Double) -> Double in
        let step = lengh / Double(self.xAxisValue.count - 1)
            return Double(step)
        }
    
    private lazy var stepY = { (height: Double) -> Double in
        return self.stepYScale(height: height)
    }
    
    lazy var stepYPreview = { (height: Double) -> Double in
        return self.stepYScale(height: height)
        
    }
    
    func stepYScale(height: Double) -> Double {
//        let dictWithMax = self.columns.max(by: {a, b in a.value.max()! < b.value.max()!})
//        let maxY = (dictWithMax?.value.max())!
//        let step = height / Double(maxY)
        return 0
    }
    
    required init?(JSON: [String:AnyObject]) {
        
        guard let columnsJ = JSON["columns"] as? [[AnyObject]] else { return nil }
        guard let typesJ = JSON["types"] as? StringDictionary else { return nil }
        guard let namesJ = JSON["names"] as? StringDictionary else { return nil }
        guard let colorsJ = JSON["colors"] as? StringDictionary else { return nil }

        self.colors = columnsColor(value: colorsJ)
        self.columns = columnsData(value: columnsJ.filter({($0[0] as! String) != "x"}), colors: columnsColor(value: colorsJ), names: namesJ, type: typesJ)
//        self.columns = columnsData(value: columnsJ, colors: columnsColor(value: colorsJ), names: namesJ, type: typesJ)
        self.columnsId = columnsJ.filter({($0[0] as! String) != "x"}).map({$0.first}) as! [String]
        self.xAxisValue = xAxisData(value: columnsJ.filter({($0[0] as! String) == "x"}))
        self.types = typesJ
        self.names = namesJ
        
        self.y_scaled = readOptionalParam(JSON: JSON, param: "y_scaled")
        self.stacked = readOptionalParam(JSON: JSON, param: "stacked")
        self.percentage = readOptionalParam(JSON: JSON, param: "percentage")
    }
    
    func readOptionalParam(JSON: [String:AnyObject], param: String) -> Bool {
        guard let retVal = JSON[param] as? Bool else { return false }
        return retVal
    }
    
    var count: Int {
        get {
            return columns.count
        }
    }
    
    func columnsData(value: [[AnyObject]], colors: ColorParam, names: StringDictionary, type: StringDictionary) -> [LineEntry] {
        
        if value.isEmpty {
            return []
        }
        
        var retVal: [LineEntry] = []
        
        for column in value {
            
            guard let key = column[0] as? String else { return [] }
            
            if let arrValue = Array(column.suffix(column.count - 1)) as? [Double] {
                retVal.append(LineEntry(id: key, name: names[key]!, height: arrValue, type: typeCharts(type: type[key]!), color: colors[key]!, turnOn: true))
            }
        }
        
        return retVal
    }
    
    func typeCharts(type: String) -> TypeChart {
        
        switch type {
        case "line":
            return TypeChart.line
        case "area":
            return TypeChart.area
        case "bar":
            return TypeChart.bar
        case "x":
            return TypeChart.x
        default:
            return TypeChart.line
        }
    }
    
    func xAxisData(value:[[AnyObject]]) -> [Date] {
        if value.isEmpty {
            return []
        }
        
        var retVal: [Date] = []
        
        for column in value {
            
            if let arrValue = Array(column.suffix(column.count - 1)) as? [Int64] {
                retVal = arrValue.map({Date(timeIntervalSince1970: Double($0) / 1000)})
            }
        }
        
        return retVal
    }
    
    func columnsColor(value: StringDictionary) -> ColorParam {
        
        let retVal = value.mapValues({UIColor(netHex: Int($0.replacingOccurrences(of: "#", with: ""), radix: 16)!)})
        
        return retVal
    }
    
    func getStepX(lengh: Double) -> Double {
        return stepX(lengh)
    }
    
    func getStepY(height: Double) -> Double {
        return stepY(height)
    }
    
    func getStepYPreview(height: Double) -> Double {
        return stepYPreview(height)
    }
    
    deinit {
        print("destroy")
    }
}

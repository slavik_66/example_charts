//
//  UIColorExt.swift
//  CalendarEmploee
//
//  Created by slavik_66 on 05.09.2018.
//  Copyright © 2018 slavik_66. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0);
    }
    
    convenience init(netHex: Int) {
        self.init(red:(netHex >> 16) & 0xFF, green:(netHex >> 8) & 0xFF, blue:netHex & 0xFF)
    }
    
    struct customColor {
        struct gray {
            static let touchBtnDown = UIColor(netHex: 0xebe9e7)
            static let sysBarColor = UIColor(netHex: 0xf9f9f9)
        }
        
        static let darkTheme = UIColor(netHex: 0x292B35)
        static let textColor = UIColor(netHex: 0x48484a)
        static let backGround = UIColor(netHex: 0xEBEBEB)
        static let lightBlack = UIColor(netHex: 0x1c1c1c)
    }
}

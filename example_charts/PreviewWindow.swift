//
//  PreviewWindow.swift
//  example_charts
//
//  Created by slavik_66 on 14/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class PreviewWindow: UIView {
    
    private let leftLayerArrow = CALayer()
    private let rightLayerArrow = CALayer()

    private let rightArrow = UIView()
    private let leftArrow = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        
        layer.cornerRadius = 5
        layer.borderWidth = 0.2
        layer.masksToBounds = true
        
        isOpaque = false
        
        leftLayerArrow.backgroundColor = UIColor.lightGray.cgColor
        leftLayerArrow.opacity = 0.8
        leftLayerArrow.isOpaque = false
        layer.addSublayer(leftLayerArrow)
        
        rightLayerArrow.backgroundColor = UIColor.lightGray.cgColor
        rightLayerArrow.opacity = 0.8
        rightLayerArrow.isOpaque = false
        layer.addSublayer(rightLayerArrow)
        
        setRightArrow()
        setLeftArrow()
    }
    
    func setRightArrow() {
        
        addSubview(rightArrow)
        
        rightArrow.isOpaque = false
        rightArrow.backgroundColor = .gray
        
        rightArrow.translatesAutoresizingMaskIntoConstraints = false
        
        let rightConstraint = NSLayoutConstraint(item: rightArrow, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let widthConstraint = rightArrow.widthAnchor.constraint(equalToConstant: 14)
        let heightConstraint = rightArrow.heightAnchor.constraint(equalToConstant: 46)
//        let leftConstraint = NSLayoutConstraint(item: rightArrow, attribute: .leading, relatedBy: .lessThanOrEqual, toItem: self, attribute: .leading, multiplier: 1, constant: frame.width - 28)
        
        addConstraints([rightConstraint, widthConstraint, heightConstraint])
        
        let pathRightArrow = UIBezierPath()
        
        pathRightArrow.move(to: CGPoint(x: 5, y: 15))
        pathRightArrow.addLine(to: CGPoint(x: 10, y: 22))
        pathRightArrow.addLine(to: CGPoint(x: 5, y: 29))
        
        let arrowLayerRight = CAShapeLayer()
        arrowLayerRight.path = pathRightArrow.cgPath
        arrowLayerRight.fillColor = UIColor.clear.cgColor
        arrowLayerRight.strokeColor = UIColor.black.cgColor
        arrowLayerRight.isOpaque = false
        arrowLayerRight.lineWidth = 2
        rightArrow.layer.addSublayer(arrowLayerRight)
    }
    
    func setLeftArrow() {
        
        addSubview(leftArrow)
        
        leftArrow.isOpaque = false
        leftArrow.backgroundColor = .gray
        
        leftArrow.translatesAutoresizingMaskIntoConstraints = false
        
        let rightConstraint = NSLayoutConstraint(item: leftArrow, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let widthConstraint = leftArrow.widthAnchor.constraint(equalToConstant: 14)
        let heightConstraint = leftArrow.heightAnchor.constraint(equalToConstant: 46)
        
        addConstraints([rightConstraint, widthConstraint, heightConstraint])
        
        let pathLeftArrow = UIBezierPath()
        
        pathLeftArrow.move(to: CGPoint(x: 10, y: 15))
        pathLeftArrow.addLine(to: CGPoint(x: 5, y: 22))
        pathLeftArrow.addLine(to: CGPoint(x: 10, y: 29))
        
        let arrowLayerLeft = CAShapeLayer()
        arrowLayerLeft.path = pathLeftArrow.cgPath
        arrowLayerLeft.fillColor = UIColor.clear.cgColor
        arrowLayerLeft.strokeColor = UIColor.black.cgColor
        arrowLayerLeft.isOpaque = false
        arrowLayerLeft.lineWidth = 2
        
        leftArrow.layer.addSublayer(arrowLayerLeft)
    }
    
    func resize(value: CGFloat) {
        rightArrow.center.x -= value
    }
    
    deinit {
        print("destroy")
    }

}

//
//  ButtonControl.swift
//  example_charts
//
//  Created by slavik_66 on 24/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class ButtonControl: UIButton {
    
    private var colorChart: UIColor = UIColor.clear
    var name: String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(_name: String, _color: UIColor, _title: String) {
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.masksToBounds = true
        colorChart = _color
        layer.borderColor = _color.cgColor
        On = true
        setTitle(_title, for: .normal)
        name = _name
    }
    
    var On: Bool = true {
        didSet {
            if On {
                layer.backgroundColor = colorChart.cgColor
                setTitleColor(.white, for: .normal)
            } else {
                layer.backgroundColor = UIColor.clear.cgColor
                setTitleColor(colorChart, for: .normal)
            }
        }
    }

}

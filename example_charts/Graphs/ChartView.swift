//
//  GraphsView.swift
//  example_charts
//
//  Created by slavik_66 on 24/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class ChartView: UIView {
    
    func drawLayer(layerDraw: CALayer) {
        layer.addSublayer(layerDraw)
    }
    
    func cleanDraw() {
        layer.sublayers?.forEach({$0.removeFromSuperlayer()})
    }
    
}

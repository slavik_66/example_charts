//
//  LineEntry.swift
//  example_charts
//
//  Created by slavik_66 on 13/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

enum TypeChart {
    case line
    case area
    case bar
    case x
}

struct LineEntry {
    let id: String
    let name: String
    var height: [Double]
    let type: TypeChart
    let color: UIColor?
    var turnOn = true
}

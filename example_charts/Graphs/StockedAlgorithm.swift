//
//  StockedAlgorithm.swift
//  example_charts
//
//  Created by slavik_66 on 13/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class StockedAlgorithm {
    static let shared = StockedAlgorithm()
    
    func createLinePath(inputData: [Double], startX: Double, startY: Double, stepX: Double, stepY: Double) -> UIBezierPath? {
        
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: startX, y: startY - stepY * inputData[0]))
        
        for i in 1..<inputData.count {
            path.addLine(to: CGPoint(x: stepX * Double(i), y: startY - stepY * inputData[i]))
        }
        
        return path
    }
    
    func createBarPath(inputData: [Double], startX: Double, startY: Double, stepX: Double, stepY: Double, bottomCoordinate: inout [Double]) -> UIBezierPath? {
        
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: startX, y: startY - bottomCoordinate[0] - stepY * inputData[0]))
        path.addLine(to: CGPoint(x: stepX + startX, y: startY - bottomCoordinate[0] - stepY * inputData[0]))
        
        for j in 1..<inputData.count {
            path.addLine(to: CGPoint(x: stepX * Double(j), y: startY - bottomCoordinate[j] - stepY * inputData[j]))
            path.addLine(to: CGPoint(x: stepX + stepX * Double(j), y: startY - (bottomCoordinate[j]) - stepY * inputData[j]))
        }
        
        for j in (1..<inputData.count).reversed() {
            path.addLine(to: CGPoint(x: stepX + stepX * Double(j), y: startY - (bottomCoordinate[j])))
            path.addLine(to: CGPoint(x: stepX * Double(j), y: startY - (bottomCoordinate[j])))
            bottomCoordinate[j] += stepY * inputData[j]
        }
        
        path.addLine(to: CGPoint(x: stepX + startX, y: startY - (bottomCoordinate[0])))
        path.addLine(to: CGPoint(x: startX, y: startY - (bottomCoordinate[0])))
        bottomCoordinate[0] += stepY * inputData[0]
        
        path.close()
        
        return path
    }
    
    func craeteLineAreaPath(inputData: [Double], startX: Double, startY: Double, stepX: Double, bottomCoordinate: inout [Double], maxValues: [Double]) -> UIBezierPath? {
        
        let path = UIBezierPath()
        
        var stepY = startY / maxValues[0]
        
        path.move(to: CGPoint(x: startX, y: startY - stepY * inputData[0]))
        
        for i in 0..<inputData.count {
            stepY = startY / maxValues[i]
            path.addLine(to: CGPoint(x: stepX * Double(i), y: startY - bottomCoordinate[i] - stepY * inputData[i]))
        }
        
        for j in (0..<inputData.count).reversed() {
            stepY = startY / maxValues[j]
            path.addLine(to: CGPoint(x: stepX * Double(j), y: startY - bottomCoordinate[j]))
            bottomCoordinate[j] += stepY * inputData[j]
        }
        
        path.close()
        
        return path
    }
    
}

//
//  ChartCell.swift
//  example_charts
//
//  Created by slavik_66 on 08/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class ChartCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var chartView: ChartView!
    @IBOutlet weak var chartPreview: ChartView!
    @IBOutlet weak var selectWindow: PreviewWindow!
    
    private var valueHeight: Double = 0
    private var bottomFreeSpace: Double = 25
    private var valueStepX: Double = 0
    private var valueStepXFilter: Double = 0
    private var panGestureAnchorPoint: CGPoint?
    private var isResizeLeft = false
    private var isResizeRight = false
    private var xAxisValueFilter: [Date] = [Date]()
    private var dataEntriesFilter: [LineEntry]!
    private var detailView = DetailWindow()
    private var arrBtns = [ButtonControl]()
    
    typealias TuplesParamDraw = (startY: Double, stepX: Double, stepY: Double)
    
    var xAxisValue: [Date] = [Date]()
    var y_scaled = false
    var stacked = false
    var percentage = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Detail windows param.
        addSubview(detailView)
        detailView.isHidden = true
        detailView.layer.cornerRadius = 10
        detailView.layer.masksToBounds = true
        detailView.layer.borderWidth = 0.2
        detailView.isOpaque = false
        detailView.alpha = 0.85
        detailView.backgroundColor = #colorLiteral(red: 0.9469092488, green: 0.941280067, blue: 0.9512361884, alpha: 1)
        
        // User interaction.
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(moveResizePreview))
        gesture.minimumPressDuration = 0
        gesture.delaysTouchesBegan = false
        selectWindow.addGestureRecognizer(gesture)
        
        let selectGesture = UITapGestureRecognizer(target: self, action: #selector(tapSelectChartValue))
        chartView.addGestureRecognizer(selectGesture)
        
        let tapDetailGesture = UITapGestureRecognizer(target: self, action: #selector(tapDetailWindow))
        detailView.addGestureRecognizer(tapDetailGesture)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanDraw()
        selectWindow.frame = chartPreview.frame
        detailView.isHidden = true
    }
    
    var dataEntries: [LineEntry]? = nil {
        didSet {
            if let dataEntries = dataEntries {
                valueHeight = Double(chartView.frame.height) - bottomFreeSpace
                valueStepX = Double(chartPreview.frame.width) / Double(xAxisValue.count)
                showCharts(inputData: dataEntries, numberPoint: xAxisValue.count, chartViewer: chartPreview)
                setFilterCharts()
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Calculate value for draw
    private func maxValueLine(inputData: [LineEntry]) -> Double {
        
        var value: Double = 0
        
        inputData.forEach {
            value = max(value, $0.height.max()!)
        }
        
        return value
    }
    
    private func maxValueArea(number: Int, inputData: [LineEntry]) -> [Double] {
        
        var arrReturn = [Double](repeating: 0, count: number)
        
        for i in 0..<number {
            for item in inputData {
                arrReturn[i] += item.height[i]
            }
        }
        
        return arrReturn
    }
    
    private func getParamDraw(widthLayer: Double, numberPoint: Double, maxY: Double, freeBottomSpace: Double, chartViewer: ChartView) -> TuplesParamDraw {
        
        let startY = Double(chartViewer.frame.height) - freeBottomSpace
        let stepX = widthLayer / numberPoint
        let stepY = startY / maxY
        
        return (startY, stepX, stepY)
    }
    
    private func numberInShortStr(number: Int) -> String {
        var retStr = String(number) == "0" ? "" : String(number)
        
        let remainder = number % 1000
        
        if remainder != number {
            let thousands = (number - remainder) / 1000
            let hundreds = Int(remainder / 100)
            
            if hundreds > 0 {
                retStr = String(thousands) + "." + String(hundreds) + "K"
            } else {
                retStr = String(thousands) + "K"
            }
            
        }
        
        return retStr
    }
    
    private func setFilterCharts() {
        
        let startPos = Int(Double(selectWindow.frame.minX - 4) / valueStepX)
        let endPos = Int(Double(selectWindow.frame.maxX - 4) / valueStepX) - 1
        dataEntriesFilter = dataEntries!.filter({$0.turnOn})

        for i in 0..<(dataEntriesFilter!.count) {
            dataEntriesFilter[i].height = Array(dataEntriesFilter[i].height[startPos...endPos])
        }
        
        xAxisValueFilter = Array(xAxisValue[startPos...endPos])
        valueStepXFilter = Double(chartView.frame.width) / Double(xAxisValueFilter.count)
        
        chartView.cleanDraw()
        showCharts(inputData: dataEntriesFilter, numberPoint: xAxisValueFilter.count, chartViewer: chartView, bottomSpace: bottomFreeSpace)
        drawHorizontalLines()
        
        // Display marker date and values
        let maxYValueLine = maxValueLine(inputData: dataEntriesFilter)
        let maxYValueBar = maxValueArea(number: endPos - startPos + 1, inputData: dataEntriesFilter).max()!
        
        let paramDrawMarker = getParamDraw(widthLayer: Double(chartView!.frame.width), numberPoint: 1, maxY: 6, freeBottomSpace: bottomFreeSpace, chartViewer: chartView)
        if y_scaled {
            
            switch dataEntriesFilter?.count {
            case 2:
                let rightValue = dataEntriesFilter[1]
                let maxRight = rightValue.turnOn ? rightValue.height.max()!/6 : 0
                displayMarkerValues(stepValue: maxRight, frameStartX: Double(chartView.frame.width - 30), stepLine: paramDrawMarker.stepY, color: rightValue.color!)
                fallthrough
            case 1:
                let leftValue = dataEntriesFilter[0]
                let maxLeft = leftValue.turnOn ? leftValue.height.max()!/6 : 0
                displayMarkerValues(stepValue: maxLeft, frameStartX: 5, stepLine: paramDrawMarker.stepY, color: leftValue.color!)
            default:
                break
            }
        } else if stacked && !percentage {
            let stepValue = maxYValueBar / 6
            displayMarkerValues(stepValue: stepValue, frameStartX: 5, stepLine: paramDrawMarker.stepY)
        } else if percentage {
            let stepValue = Double(100 / 6)
            displayMarkerValues(stepValue: stepValue, frameStartX: 5, stepLine: paramDrawMarker.stepY)
        } else {
            let stepValue = Double(maxYValueLine / 6)
            displayMarkerValues(stepValue: stepValue, frameStartX: 5, stepLine: paramDrawMarker.stepY)
        }
        
        displayMarkerDate(startPos: startPos, endPos: endPos)
        setTitlePeriod(beginDate: xAxisValueFilter.first!, endDate: xAxisValueFilter.last!)
    }
    
    // MARK: - Draw charts
    func showCharts(inputData: [LineEntry], numberPoint: Int, chartViewer: ChartView, bottomSpace: Double = 0) {
        
        if inputData.isEmpty { return }
        
        let maxYValueLine = maxValueLine(inputData: inputData)
        let maxYValueBar = maxValueArea(number: numberPoint, inputData: inputData).max()!
        let maxYValueArea = maxValueArea(number: numberPoint, inputData: inputData)
        var bottomCoordinate = [Double](repeating: 0, count: numberPoint)
        
        let paramDrawBar = getParamDraw(widthLayer: Double(chartViewer.frame.width), numberPoint: Double(numberPoint), maxY: maxYValueBar, freeBottomSpace: bottomSpace, chartViewer: chartViewer)
        let paramDrawArea = getParamDraw(widthLayer: Double(chartViewer.frame.width), numberPoint: Double(numberPoint), maxY: maxYValueBar, freeBottomSpace: bottomSpace, chartViewer: chartViewer)
        let paramDraw = getParamDraw(widthLayer: Double(chartViewer.frame.width), numberPoint: Double(numberPoint), maxY: maxYValueLine, freeBottomSpace: bottomSpace, chartViewer: chartViewer)
        
        inputData.forEach {
            switch $0.type {
            case .line:
                if y_scaled {
                    let paramDraw = getParamDraw(widthLayer: Double(chartViewer.frame.width), numberPoint: Double($0.height.count), maxY: $0.height.max()!, freeBottomSpace: bottomSpace, chartViewer: chartViewer)
                    drawLineChart(inputData: $0.height, color: $0.color!, chartViewer: chartViewer, paramDraw: paramDraw)
                } else {
                    drawLineChart(inputData: $0.height, color: $0.color!, chartViewer: chartViewer, paramDraw: paramDraw)
                }
            case .bar:
                drawEntryBar(inputData: $0.height, color: $0.color!, chartViewer: chartViewer, paramDraw: paramDrawBar, bottomCoordinate: &bottomCoordinate)
            case .area:
                drawEntryBarArea(inputData: $0, chartViewer: chartViewer, maxYValues: maxYValueArea, paraDraw: paramDrawArea, bottomCoordinate: &bottomCoordinate)
            default:
                break
            }
        }
    }
    
    private func drawLineChart(inputData: [Double], color: UIColor, chartViewer: ChartView, paramDraw: TuplesParamDraw, startX: Double = 0) {
        
        if let path = StockedAlgorithm.shared.createLinePath(inputData: inputData, startX: startX, startY: paramDraw.startY, stepX: paramDraw.stepX, stepY: paramDraw.stepY) {
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.fillColor = UIColor.clear.cgColor
            lineLayer.backgroundColor = UIColor.clear.cgColor
            lineLayer.strokeColor = color.cgColor
            lineLayer.lineWidth = 0.5
            chartViewer.drawLayer(layerDraw: lineLayer)
        }
    }
    
    private func drawEntryBar(inputData: [Double], color: UIColor, chartViewer: ChartView, paramDraw: TuplesParamDraw, bottomCoordinate: inout [Double], startX: Double = 0) {
        
        if let path = StockedAlgorithm.shared.createBarPath(inputData: inputData, startX: startX, startY: paramDraw.startY, stepX: paramDraw.stepX, stepY: paramDraw.stepY, bottomCoordinate: &bottomCoordinate) {
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.fillColor = color.cgColor
            lineLayer.backgroundColor = UIColor.clear.cgColor
            lineLayer.strokeColor = UIColor.clear.cgColor
            lineLayer.lineWidth = 0.5
            chartViewer.drawLayer(layerDraw: lineLayer)
        }
    }
    
    private func drawEntryBarArea(inputData: LineEntry, chartViewer: ChartView, maxYValues: [Double], paraDraw: TuplesParamDraw, bottomCoordinate: inout [Double], startX: Double = 0) {
        
        if let path = StockedAlgorithm.shared.craeteLineAreaPath(inputData: inputData.height, startX: startX, startY: paraDraw.startY, stepX: paraDraw.stepX, bottomCoordinate: &bottomCoordinate, maxValues: maxYValues) {
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.fillColor = inputData.color!.cgColor
            lineLayer.backgroundColor = UIColor.clear.cgColor
            lineLayer.strokeColor = UIColor.clear.cgColor
            lineLayer.lineWidth = 0.5
            chartViewer.drawLayer(layerDraw: lineLayer)
        }
    }
    
    private func drawHorizontalLines() {
        
        let paramDraw = getParamDraw(widthLayer: Double(chartView!.frame.width), numberPoint: 1, maxY: 6, freeBottomSpace: bottomFreeSpace, chartViewer: chartView)
        
        for i in 0...5 {
            drawLineChart(inputData: [Double(i), Double(i)], color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), chartViewer: chartView, paramDraw: paramDraw)
        }
    }
    
    private func displayMarkerDate(startPos: Int, endPos: Int) {

        let areaMarker = Double(chartView.frame.width) / 5
        let xAxisFilter = Array(xAxisValue[startPos...endPos])
        let stepX = Double(chartView.frame.width) / Double(xAxisFilter.count)
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "d MMM yy"
        
        for i in 1...5 {
            
            let currentX = areaMarker * Double(i)
            let currentIndex = Int(currentX / stepX) - 1
            
            let str = dateFormater.string(from: xAxisFilter[currentIndex])
            
            let newTextLayer = CATextLayer()
            newTextLayer.font = UIFont(name: "System", size: 12)
            newTextLayer.frame = CGRect(x: currentX - areaMarker / 2, y: Double(chartView.frame.height) - bottomFreeSpace, width: areaMarker , height: 20)
            newTextLayer.string = str
            chartView.drawLayer(layerDraw: newTextLayer)
        }
    }
    
    private func displayMarkerValues(stepValue: Double, frameStartX: Double, stepLine: Double, color: UIColor = UIColor.darkText) {
        for i in 1...5 {
            let layerText = CATextLayer()
            let str = numberInShortStr(number: Int(stepValue * Double(i)))
            layerText.foregroundColor = color.cgColor
            layerText.fontSize = 10
            layerText.frame = CGRect(x: frameStartX, y: Double(chartView.frame.height) - bottomFreeSpace - stepLine * Double(i) - 15, width: 35, height: 15)
            layerText.string = str
            chartView.drawLayer(layerDraw: layerText)
        }
    }
    
    private func setTitlePeriod(beginDate: Date, endDate: Date) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "d MMMM y"
        let strDateBegin = dateFormater.string(from: beginDate)
        let strDateEnd = dateFormater.string(from: endDate)
        
        let layerText = CATextLayer()
        layerText.string = strDateBegin + " - " + strDateEnd
        layerText.font = UIFont(name: "System", size: 10)
        layerText.frame = CGRect(x: Double(chartView.frame.midX) - 125, y: 0, width: 250, height: 15)
        layerText.alignmentMode = .center
        chartView.drawLayer(layerDraw: layerText)
    }
    
    private func cleanDraw() {
        chartView.cleanDraw()
        chartPreview.cleanDraw()
        arrBtns.forEach({$0.isHidden = true})
    }
    
    func setTurnOnButton() {
        
        let widthBtn = Double((chartPreview.frame.maxX - 16) / 3)
        let heightBtn = 33.0
        var startX = 4.0
        var startY = Double(chartPreview.frame.maxY + 4)
        var item = 0
        
        let rows = Int(ceil(Double(dataEntries!.count) / 3))
        
        for i in 0..<rows {
            let columns = max(min(3, dataEntries!.count - 3 * i), dataEntries!.count % 3)
            for j in 0..<columns {
                
                var newBtn: ButtonControl?
                
                if arrBtns.indices.contains(3 * i + j) {
                    newBtn = arrBtns[3 * i + j]
                    newBtn?.isHidden = false
                } else {
                    newBtn = ButtonControl()
                    arrBtns.append(newBtn!)
                    newBtn!.addTarget(self, action: #selector(btnTurnTap), for: .touchUpInside)
                    newBtn!.frame = CGRect(x: startX, y: startY, width: widthBtn, height: heightBtn)
                    addSubview(newBtn!)
                }
                
                newBtn!.setup(_name: dataEntries![item].id, _color: dataEntries![item].color!, _title: dataEntries![item].name)
                item += 1
                startX += widthBtn
            }
            startX = 4.0
            startY += heightBtn
        }
    }
    
    // MARK: - User interaction
    @objc private func moveResizePreview(sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .began:
            detailView.isHidden = true
            assert(panGestureAnchorPoint == nil)
            panGestureAnchorPoint = sender.location(in: selectWindow)
            
            if 0.0...14.0 ~= Double(panGestureAnchorPoint!.x) { isResizeLeft = true }
            if selectWindow.frame.width-14...selectWindow.frame.width ~= panGestureAnchorPoint!.x { isResizeRight = true }
        case .changed:
            guard let panGestureAnchorPoint = panGestureAnchorPoint else { assert(false); return }
            
            let gesturePoint = sender.location(in: selectWindow)
            let value = panGestureAnchorPoint.x - gesturePoint.x
            
            if isResizeLeft {
                
                if selectWindow.frame.minX - value < chartView.frame.minX {
                    break
                }
                
                selectWindow.frame.size.width += value
                selectWindow.center.x -= value
            } else if isResizeRight {
                
                if selectWindow.frame.maxX - value > chartView.frame.maxX {
                    break
                }
                
                selectWindow.frame.size.width -= value
                self.panGestureAnchorPoint = gesturePoint
            } else {
                if selectWindow.frame.minX - value < chartPreview.frame.minX ||
                    selectWindow.frame.maxX - value > chartPreview.frame.maxX  {
                    break
                }
                selectWindow.center.x -= value
            }
            setFilterCharts()
            selectWindow.resize(value: value)
        case .cancelled, .ended:
            panGestureAnchorPoint = nil
            isResizeRight = false
            isResizeLeft = false
        default:
            break
        }
    }
    
    @objc private func tapSelectChartValue(sender: UITapGestureRecognizer) {
        
        detailView.cleanDraw()
        setFilterCharts()
        
        let coordinatPoint = sender.location(in: chartView)
        let xCoordinatPoint = coordinatPoint.x
        
        let xPoint = max(round(Double(xCoordinatPoint) / valueStepXFilter), 0)
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "E, d MMM y"
        
        let startPointX = xCoordinatPoint > 130 ? xCoordinatPoint - 130 : xCoordinatPoint + 10
        let heightDetailWin = CGFloat(dataEntriesFilter.count * 20 + 20)
        detailView.frame = CGRect(x: startPointX, y: 50.0, width: 120.0, height: heightDetailWin)
        detailView.isHidden = false
        
        // Title detail window.
        let title = CATextLayer()
        title.font = UIFont(name: "System", size: 10)
        title.frame = CGRect(x: 0, y: 0, width: 120, height: 15)
        title.alignmentMode = .center
        title.string = dateFormater.string(from: xAxisValueFilter[Int(xPoint)])
        detailView.drawLayer(layerDraw: title)
        
        // Fill details window
        for i in 1...dataEntriesFilter.count {
            let dataText = CATextLayer()
            let str = dataEntriesFilter[i-1].name + "   " + String(dataEntriesFilter[i-1].height[Int(xPoint)])
            dataText.string = str
            dataText.fontSize = 12
            dataText.foregroundColor = dataEntriesFilter[i-1].color?.cgColor
            dataText.frame = CGRect(x: 5.0, y: Double(20 * i), width: Double(detailView.frame.width) - 10.0, height: 15.0)
            detailView.drawLayer(layerDraw: dataText)
        }
        
        // Display select date line
        if stacked {
            
            let param = (Double(chartView.frame.height) - bottomFreeSpace, xPoint * valueStepXFilter / 2, 1.0)
            var bottomCoordinate = [Double](repeating: 0, count: 2)
            drawEntryBar(inputData: [Double](repeating: Double(chartView.frame.height) - bottomFreeSpace, count: 2), color: #colorLiteral(red: 0.7489061952, green: 0.7444557548, blue: 0.7523280382, alpha: 0.705078125), chartViewer: chartView, paramDraw: param, bottomCoordinate: &bottomCoordinate)
            
            let param1 = (Double(chartView.frame.height) - bottomFreeSpace, Double(chartView.frame.width) - xPoint * valueStepXFilter / 2, 1.0)
            bottomCoordinate = [Double](repeating: 0, count: 2)
            drawEntryBar(inputData: [Double](repeating: Double(chartView.frame.height) - bottomFreeSpace, count: 2), color: #colorLiteral(red: 0.7489061952, green: 0.7444557548, blue: 0.7523280382, alpha: 0.705078125), chartViewer: chartView, paramDraw: param1, bottomCoordinate: &bottomCoordinate, startX: xPoint * valueStepXFilter + valueStepXFilter)
            
        } else {
            let param = getParamDraw(widthLayer: round(xPoint) * valueStepXFilter * 2, numberPoint: 2, maxY: Double(chartView.frame.height) - bottomFreeSpace, freeBottomSpace: bottomFreeSpace, chartViewer: chartView)
            drawLineChart(inputData: [Double(0), Double(chartView.frame.height) - bottomFreeSpace], color: #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1), chartViewer: chartView, paramDraw: param, startX: xPoint * valueStepXFilter)
        }
        
    }
    
    @objc private func tapDetailWindow(sender: UITapGestureRecognizer) {
        detailView.isHidden = true
        setFilterCharts()
    }
    
    @objc private func btnTurnTap(sender: ButtonControl) {
        
        detailView.isHidden = true
        chartView.cleanDraw()
        chartPreview.cleanDraw()
        
        sender.On = !sender.On
        
        for i in 0..<dataEntries!.count {
            if dataEntries![i].id == sender.name {
               dataEntries![i].turnOn = sender.On
            }
        }
    }
}

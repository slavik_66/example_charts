//
//  ManagerJSON.swift
//  example_charts
//
//  Created by slavik_66 on 10/03/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit
import Foundation


class ManagerJSON {
    
    // Read JSON
    func readJSON(nameFiles: String) -> [String:AnyObject] {
        
        var retval = [String:AnyObject]()
        
        do {
            if let file = Bundle.main.url(forResource: nameFiles, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let object = json as? [String:AnyObject] {
                    retval = object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return retval
    }
    
}

//
//  ViewController.swift
//  example_charts
//
//  Created by slavik_66 on 07/04/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate,
                    UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var listOfGraphs: UITableView!
    
    var arrChart = [ChartsData]()
    
    private var firstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        listOfGraphs.delegate = self
        listOfGraphs.dataSource = self
        
        let managerJSON = ManagerJSON()
        
        for i in 1...5 {
            arrChart.append(ChartsData(JSON: managerJSON.readJSON(nameFiles: String(i) + "_charts"))!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstLoad = false
        listOfGraphs.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = listOfGraphs.dequeueReusableCell(withIdentifier: "ChartCell", for: indexPath) as! ChartCell
 
        switch indexPath.row {
        case 1:
            cell.label.text = "INTERACTIONS"
        case 2:
            cell.label.text = "MESSAGES"
        case 3:
            cell.label.text = "VIEWS"
        case 4:
            cell.label.text = "APPS"
        default:
            cell.label.text = "FOLOWERS"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if firstLoad { return }
        
        if let cell = cell as? ChartCell {
            cell.xAxisValue = arrChart[indexPath.row].xAxisValue
            cell.y_scaled = arrChart[indexPath.row].y_scaled
            cell.stacked = arrChart[indexPath.row].stacked
            cell.percentage = arrChart[indexPath.row].percentage
            cell.dataEntries = arrChart[indexPath.row].columns
            cell.setTurnOnButton()
        }
    }
    
}


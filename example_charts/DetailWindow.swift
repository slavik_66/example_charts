//
//  DetailWindow.swift
//  example_charts
//
//  Created by slavik_66 on 03/05/2019.
//  Copyright © 2019 slavik_66. All rights reserved.
//

import UIKit

class DetailWindow: UIView {

    func cleanDraw() {
        layer.sublayers?.forEach {$0.removeFromSuperlayer()}
    }
    
    func drawLayer(layerDraw: CALayer) {
        layer.addSublayer(layerDraw)
    }
    
}
